#/usr/bin/python
''' 
    Line simsimi
    @Author: Irsyad Fauzan <irsyad.fauzan777@gmail.com>
    @Date: 17 September 2014
'''
import json
import urllib
import urllib2
from cookielib import CookieJar
from line import LineClient, LineGroup, LineContact

user_agent = "Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:24.0) Gecko/20100101 Firefox/24.0"
cj = CookieJar()
opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))
opener.addheaders = [("Accept", "application/json, text/javascript, */*; q=0.01")]
opener.addheaders = [("User-Agent", user_agent)]
opener.addheaders = [("X-Requested-With", "XMLHttpRequest")]
opener.addheaders = [("Referer", "http://www.simsimi.com/")]
opener.addheaders = [("Cookie", "Filtering=0.0; isFirst=1; fbm_370812466271816=base_domain=.simsimi.com; selected_nc=id; sim_name=tamu; sid=s%3An0BoEwDJLxZN7jYHyBTYt4CO.IXeRNmoQrVgBms6tN%2FqIvmSMEIyU7OJv83txKoK7SZ0; AWSELB=BF8D19F26622D89F6936CA1E73D2A1C3FB179428F4F0CA4CAC4DB6CBBF57A0DC51A0A73D169DA67EADBD0E315793255E4F17140D6D78FFBF26D89F31922EEAE5A2F1172A; selected_nc=id; __utma=119922954.72911278.1410948735.1410948735.1410948735.1; __utmb=119922954.26.8.1410949449006; __utmc=119922954; __utmz=119922954.1410948735.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); simsimi_uid=73062141; simsimi_uid=73062141")]

try:
	#client = LineClient("poticous777@gmail.com", "*****")
	client = LineClient(authToken="DKeAsSVRjU3oJaTqTf2b.sxhWUx6DcRWeup1nxHm56W.faCUOnnyqMIVNP2/GcTxG3fOq5qfTfKvE36HMi/Th90=")
except:
	print "Login Failed"

while True:
	op_list = []	
	try:
		for op in client.longPoll():
			op_list.append(op)

		for op in op_list:
			sender   = op[0]
			receiver = op[1]
			message  = op[2]
			
			if message.text:
				data = {'lc':'id', 'ft':'0.0', 'req':message.text, 'fl':'http://www.simsimi.com/talk.htm'}
				url = "http://www.simsimi.com/func/reqN?%s" % urllib.urlencode(data)
				res = json.loads(opener.open(url).read())
				msg = res['sentence_resp']
				receiver.sendMessage("%s" % msg)
	except Exception: 
		pass
